﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    //[RoutePrefix("values")]
    public class ValuesController : ApiController
    {
        #region DO NOT CHANGE

        private static Random _gen = new Random();
        private static List<dynamic> _list = new List<dynamic>(new[] { 1, 2, 3, 4, 5 }.Select(i => new
        {
            face = "img/cat.jpg",
            what = "Brunch this weekend?",
            who = "Min Li Chan",
            when = DateTime.Now.AddDays(-1).AddSeconds(_gen.NextDouble() * i),
            notes = "I'll be in your neighborhood doing errands"
        }));

        #endregion

        // GET api/values
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_list);
        }

        [HttpPost]
        // POST api/values
        public void Post([FromBody]dynamic value)
        {
            System.Threading.Thread.Sleep(5000);
            _list.Add(new
            {
                face = "img/cat.jpg",
                what = value.what,
                who = "Min Li Chan",
                when = DateTime.Now,
                notes = value.note
            });
        }

    }
}
