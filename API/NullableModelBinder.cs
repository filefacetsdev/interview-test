﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace API
{
    public class NullableModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext modelBindingExecutionContext, ModelBindingContext bindingContext)
        {
            var val = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (val == null)
                return false;

            var rawVal = val.RawValue as string;

            if (rawVal == null)
                return false;

            var converter = TypeDescriptor.GetConverter(bindingContext.ModelType);

            if (converter is StringConverter && rawVal == "")
            {
                bindingContext.ValidationNode.SuppressValidation = true;
                return true;
            }
            else if (converter is NullableConverter && (rawVal == "null" || rawVal == ""))
            {
                bindingContext.ValidationNode.SuppressValidation = true;
                return true;
            }
            else if (converter.IsValid(rawVal))
            {
                bindingContext.Model = converter.ConvertFromString(rawVal);
                return true;
            }

            return false;
        }
    }
}