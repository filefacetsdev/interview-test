﻿
angular.module('MyApp', ['ngMaterial', 'ngMessages'])
.config(function () {
})
.controller('AppCtrl', function ($http, $scope) {
    $scope.what = null;
    $scope.notes = null;

    var getToDo = function () {
        return $http.get('/api/values?v=' + Math.random());
    };

    $scope.createMessage = function () {
        var result = $http.post('/api/values?v=' + Math.random(), { what: $scope.what, notes: $scope.notes })
        getToDo().then(function (resultGet) {
            $scope.todos = resultGet.data;
        });

        return result;
    }

    getToDo().then(function (result) {
        $scope.todos = result.data;
    });


});